require 'test_helper'

class ApplicationHelperTest < ActionView::TestCase
     title_partial = 'Ruby on Rails Tutorial Sample App'


  test "full title helper" do
    assert_equal full_title, "#{title_partial}"
    assert_equal full_title("Help"), "Help \| #{title_partial}"
  end
end